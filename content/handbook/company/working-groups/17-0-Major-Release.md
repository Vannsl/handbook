---
title: "17.0 Major Release Working Group"
description: "Working group to faciliate a smooth and boring Major release."
status: active
---

## Attributes

| Property       | Value                                                        |
| -------------- | ------------------------------------------------------------ |
| Date Created   | 2024-02-22                                                   |
| End Date       | 2023-07-01                                                   |
| Slack          | [#17-0-release-coordination](https://gitlab.enterprise.slack.com/archives/C04S5KLDB50) (only accessible from within the company) |
| Google Doc     | [17.0 Major Release Working Group Agenda](https://docs.google.com/document/d/1jxl4SDDeoQgR9DOCZ63_D38OglE1OGkJBq_fC_z46BA/edit?usp=sharing)|
| Epic           | [17.0 Major Release Coordination Working Group](https://gitlab.com/groups/gitlab-com/-/epics/2292) |
| Overview & Status | Open |

## Purpose

The 17.0 Major release Working Group aims to minimise any negative customer impact related to the major release and breaking change across SaaS and self manged GitLab users.

### Overview

The Major release working group will form from the point that all deprecations have been announced that are due for the major release according to the deprecation policy until we are at least one milestone past the major release and aware of any customer issues that have arisen as part of the major upgrade and have a plan of action. The rough schedule for this is outlined in the attributes table above.

### Goals

This is a non-comprehensive list of topics to be discussed.

- Minimise customer impact due to breaking change and major release.
- [Go through product deprecations and separate the high impact ones for further analysis](https://gitlab.com/groups/gitlab-com/-/epics/2293#specific-asks-for-pms).
- Prepare Support and CS for customer questions on deprecations & upgrade - [Internal Slides](https://docs.google.com/presentation/d/1YjEsBpemHC5eLfNbfSCFWQK5hA3z0iysdNx6SGBw8K4/edit#slide=id.g1ef4929c487_0_34).
- Release communications to prepare customers.
- Understand whether deprecation policy and GitLab's working interpretation are aligned / need to be changed.
- Put a plan in place to improve conditions for next major release and remove need for this working group next iteration.

### Exit Criteria

Exit Criteria are avaialable in the [17.0 Major Release Coordination Epic](https://gitlab.com/groups/gitlab-com/-/epics/2292).

## Roles and Responsibilities

| Working Group Role                       | Person                           | Title                                                          |
|------------------------------------------|----------------------------------|----------------------------------------------------------------|
| Executive Stakeholder                    | Josh Lemos (@joshlemos)           | CISO |
| Facilitator/DRI                          | Sam Wiskow (@swiskow)                 | Sr. PM, Delivery & Scalability - Infra |
| Product Management                       | Mike flouton (@mflouton )         | VP - Product |
| Member                                   | Marin Jankovski (@marin)        | Senior Director - Infrastructure, SaaS Platforms  |
| Member                                   | Fabian Zimmer (@fzimmer)        |  Product Director - SaaS Platforms    |
| Member                                   | Michele Bursi (@mbursi)  | Engineering Manager, Delivery Group |
| Member                                   | Amy Phillips (@amyphillips)  | Senior Engineering Manager, GitLab Dedicated |
| Member                                   | Steve Evangelista (@steve-evangelista)    |  Senior Product Director - Dev & Analytics |
| Member                                   | Tim Zallman (@timzallmann)  | Senior Director of Engineering, Core Development |
| Member                                   | Balasankar 'Balu' C (@balasankarc)    |  Senior Distribution Engineer |
| Member                                   | Martin Bruemmer (@mbruemmer)    |  Sr. Customer Success Engineer, EMEA |
| Member                                   | Lyle Kozloff (@lyle)    |  Director of Support, Global Readiness |
