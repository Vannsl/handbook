---

title: Elevate Programs
aliases:
- /handbook/people-group/learning-and-development/elevate

---

Use the links below to navigate to each Elevate program. Here is a useful guide to ensure you're choosing the right program:

| Program Name | Description |
| ----- | -------- |
| [Elevate](/handbook/people-group/learning-and-development/elevate-programs/elevate/) | We currently estimate next cohort of Elevate to begin in August. If you've been recently enrolled, navigate here. |
| [Elevate Applied](/handbook/people-group/learning-and-development/elevate-programs/elevate-applied/) | Continuous learning and resources for those who've earned their Elevate certification |
| [Elevate+](/handbook/people-group/learning-and-development/elevate-programs/elevate+/) | Round 1 and 2 starting in FY25 Q1 |

**Not a GitLab People Leader?** We've put together a Level Up course available to all GitLab Team Members, to share the self-paced training content for each Elevate module. You can check it out here: [Elevate Learning Materials](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/elevate-learning-materials).
